#!/bin/bash -ex
# This script creates a terraform workspace configuration file
# and sets the token from the TERRAFORM_CLOUD_TOKEN environment variable
# which should be set as a secret environment variable in the Bitbucket Pipelines settings.
cat <<EOF > "${HOME}/.terraformrc"
credentials "app.terraform.io" {
  token = "${TERRAFORM_CLOUD_TOKEN}"
}
EOF

# This is another way to set a workspace for multiple workspaces in terraform cloud
# Default workspace is no longer used.  This sets it to use the deployment variable set in repo
mkdir terraform/.terraform
echo "${ENV}" > terraform/.terraform/environment