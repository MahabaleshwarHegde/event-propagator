# README #

### Repo Setup Steps ###

Steps to set up your repository to deploy terraform and kubernetes resources.

1. Fork this repository
2. Update backend file workspaces name with the exact name of the repository.
3. Create two repository variables:
    * GOOGLE_CREDENTIALS - this variable should contain the JSON contents of the service account created
    for this repository.  Service accounts can be created in the [gcloud-iam](https://bitbucket.org/ucaas_mitel/gcloud-iam) 
    repository. 
    * TERRAFORM_CLOUD_TOKEN - this variable should contain the API key for terraform cloud in order for your
    repository to be able to create and manage its terraform state file.
4. Update main.tf file to include the project IDs for your repository to deploy to.  Then use the example for enabling
the service APIs in GCP as the first pull request and merge action.

There is an example of how to do the kubernetes deployment while we are working on providing an official CD tool.
Once the repository has been set up, overwrite this file to include information required for this new repository.

## Example README Sections ##

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact