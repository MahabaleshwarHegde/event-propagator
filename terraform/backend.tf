terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "Mitel"
    workspaces {
      prefix = "event-propagator-"
    }
  }
}