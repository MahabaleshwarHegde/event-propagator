locals {
  # Project IDs for current env (dev, alpha, prod):
  ep_project_ids = var.ep_project_ids
}

# Ops Buckets for CAS team
resource "google_storage_bucket" "bucket_ep_ops" {
  for_each = local.ep_project_ids
  name     = "${each.key}-${var.env}-ep-ops"
  location = var.locations[each.key]
  labels = {
    ticket  = "cas-703"
    product = "event_propagator"
    env     = var.env
  }
  versioning {
    enabled = true
  }
  storage_class = "STANDARD"
  project       = each.value
}