
variable "env" {
  type = string
}

variable "ep_project_ids" {
  type = map(any)
}

variable "locations" {
  type = map(any)
}
