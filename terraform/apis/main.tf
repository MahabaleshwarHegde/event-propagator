locals {
  # Project IDs for current env (dev, alpha, prod):
  project_ids = {
    na_dev   = "na-dev-event-propagator-01"
    na_alpha = "na-alpha-event-propagator-01"
    na_prod  = "na-prod-event-propagator-01"
    #au_dev   = "au-dev-event-propagator-01"
    #au_alpha = "au-alpha-event-propagator-01"
    #au_prod  = "au-prod-event-propagator-01"
  }

  # List for APIs to enable in GCP
  apis = [
    "serviceusage.googleapis.com",
    "compute.googleapis.com",
  ]

  project_api_services = flatten([
    for key in keys(local.project_ids) : [
      for api in local.apis : {
        object_key = "${key}_${api}"
        api        = api
        project_id = local.project_ids[key]
      }
    ]
  ])
}

# Certain services do not come enabled upon intital project creation, enable them for each project
# This can be created for each project environment
resource "google_project_service" "apis" {
  for_each = {
    for service in local.project_api_services : service.object_key => service
  }
  project                    = each.value.project_id
  service                    = each.value.api
  disable_dependent_services = false
  disable_on_destroy         = false
}