terraform {
  backend "remote" {
    organization = "Mitel"
    workspaces {
      prefix = "event-propagator-"
    }
  }
}