env = "dev"

ep_project_ids = {
  na = "na-dev-event-propagator-01"
  au = "au-dev-event-propagator-01"
  uk = "uk-dev-event-propagator-01"
  eu = "na-dev-event-propagator-02"
}

locations = {
  na = "US"
  au = "AUSTRALIA-SOUTHEAST1"
  uk = "EUROPE-WEST2"
  eu = "US"
}