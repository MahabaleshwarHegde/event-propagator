env = "alpha"

ep_project_ids = {
  na = "na-alpha-event-propagator-01"
}

locations = {
  na = "US"
  au = "AUSTRALIA-SOUTHEAST1"
  uk = "EUROPE-WEST2"
}