env = "prod"

ep_project_ids = {
  na = "na-prod-event-propagator-01"
  au = "au-prod-event-propagator-01"
  uk = "uk-prod-event-propagator-01"
  eu = "eu-prod-event-propagator-01"
}

locations = {
  na = "US"
  au = "AUSTRALIA-SOUTHEAST1"
  uk = "EUROPE-WEST2"
  eu = "EUROPE-WEST3"
}